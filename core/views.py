import os
from datetime import datetime
from django.contrib.auth import login, authenticate, logout, update_session_auth_hash
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import PasswordChangeForm
from django.core.mail import send_mail
from django.db.models import Q
from django.shortcuts import render, redirect, get_object_or_404, HttpResponseRedirect
from django.contrib.sites.shortcuts import get_current_site
from django.utils.encoding import force_text
from django.contrib.auth.models import User
from django.db import IntegrityError
from django.utils.http import urlsafe_base64_decode
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode
from django.views.decorators.csrf import csrf_exempt
from django.contrib import messages
from django.views.generic.base import View

from core.models import Profile, Staff, Student, Panel, Project, Date, Document
from core.render import Render
from pas.settings import LOGIN_REDIRECT_URL
from .tokens import account_activation_token
from django.template.loader import render_to_string
from django.shortcuts import render, redirect


# Create your views here.
from core.forms import SignUpForm

#view for login .all type of users in a single view
def login_view(request):
    if request.method=='POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is not None:
            if user.is_superuser:
                login(request, user)
                request.session['username'] = user.username
                return redirect('admin')
            else:
                u=User.objects.get(username=username).profile.signup_confirmation
                #print(u)
                if u == False:     #checking that is he activated his account by clicking the link that sent to his mail
                    context={'text':'not_activated','user':user}
                    return render(request,'account_activation_invalid.html',context)
                else:
                    u=User.objects.get(username=username).profile.admin_approve
                    if u == False:  #checking admin approval status
                        context = {'text': 'not_approved', 'user': user}
                        return render(request, 'account_activation_invalid.html', context)
                    else:
                        u= User.objects.get(username=username).is_staff
                        if u:                                               #if user is staff then goto staff login else into the student login
                            login(request,user)
                            request.session['username']=user.username
                            return redirect('staff')
                        else:
                            login(request, user)
                            request.session['username'] = user.username
                            return redirect('student')
        else:
            messages.warning(request, 'Invalid Username or Password')
            return redirect('login_view')
    else:
        return render(request, 'login.html')





#logout view
def logout_view(request):
    logout(request)       #dont need to delete the session variable ,because it automatically deleted when calling logout()
    #del request.session['username']  # deleting the session variable which i created in login view
    messages.success(request, 'Logged out Successfully')
    return redirect('login_view')




#view for registration
def signup_view(request):
    if request.method  == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            user = form.save()
            user.refresh_from_db()
            user.profile.first_name = form.cleaned_data.get('first_name')
            user.profile.last_name = form.cleaned_data.get('last_name')
            user.profile.email = form.cleaned_data.get('email')
            # user can't login until link confirmed
            user.is_active = False
            user.save()
            current_site = get_current_site(request)
            subject = 'Please Activate Your Account'
            # load a template like get_template()
            # and calls its render() method immediately.
            message = render_to_string('account_activation_email.html', {
                'user': user,
                'domain': current_site.domain,
                'uid': urlsafe_base64_encode(force_bytes(user.pk)).decode(),
                # method will generate a hash value with user related data
                'token': account_activation_token.make_token(user),
            })
            user.email_user(subject, message)
            return redirect('activation_sent')
    else:
        form = SignUpForm()
    return render(request, 'register.html', {'form': form})


#page used to show the mesage 'activation link is sent'
def activation_sent_view(request):
    return render(request, 'account_activation_sent.html')

def activate(request, uidb64, token):
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except (TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
    # checking if the user exists, if the token is valid.
    if user is not None and account_activation_token.check_token(user, token):
        # if valid set active true
        user.is_active = True
        # set signup_confirmation true
        user.profile.signup_confirmation = True
        user.save()
        context = {'text': "not_approved"}
        return render(request,'account_activation_invalid.html',context)
    else:
        context={'text':"invalid"}
        return render(request, 'account_activation_invalid.html',context)




#this view pointed to admin home
@csrf_exempt
@login_required(login_url=LOGIN_REDIRECT_URL)
def admin_view(request):
    username=request.session.get('username')
    check_admin=User.objects.get(username=username)
    if check_admin.is_superuser:
        first_name=check_admin.first_name
        context={
            'reg_approval':reg_approval(),
            'name':first_name
        }
        #print(reg_approval())
        return render(request,'administrator/admin_home.html',context)
    else:
        messages.warning(request,'You cant access this page')
        return redirect('login_view')


#this view pointed to student home
@csrf_exempt
@login_required(login_url=LOGIN_REDIRECT_URL)
def student_view(request):
    username = request.session.get('username')
    user = User.objects.get(username=username)
    if  not user.is_staff:     #checking the user is student
        st=Student.objects.get(user_id=user.id).admission_no
        if st:
            update=False     #his profile is complete
        else:
            update=True    #he need to complete his profile
        context={'update':update,
                 'name':user.first_name+' '+user.last_name,'student':user

                 }
        return render(request, 'student/student_home.html',context)
    else:
        messages.warning(request, 'You cant access this page')
        return redirect('login_view')



#this view pointed to staff home
@csrf_exempt
@login_required(login_url=LOGIN_REDIRECT_URL)
def staff_view(request):
    username = request.session.get('username')
    check_staff = User.objects.get(username=username).is_staff  # checking the user is student or not
    if check_staff:
        return render(request, 'staff/staff_home.html')
    else:
        messages.warning(request, 'You cant access this page')
        return redirect('login_view')






#this function is used to fetch  all the  users  who requested to complete their registration to admin
def reg_approval():
    try:
        user_to_approve=Profile.objects.filter(signup_confirmation=True,admin_approve=False)
        a=[]
        for user in user_to_approve:
            a.append(user.id)
        return User.objects.filter(id__in=a)
    except:
        return False





# this view is associated with dynamic url which helps to manage  registration requests in admin side
@login_required(login_url=LOGIN_REDIRECT_URL)
def registration_approve(request,username):
    if request.method=='POST':
        #print('Inside Post')
        logged_user = request.session.get('username')
        check_admin = User.objects.get(username=logged_user).is_superuser   #ensuring that logged in person is admin
        if check_admin:
            requested_user=User.objects.get(username=username)  #getting the requested user from User model
            #print(requested_user)
            r_user=Profile.objects.get(id=requested_user.id)    #getting the row of that requested user from Profile model
            if 'reject' in request.POST:
                if requested_user.is_staff == True:
                    s = Staff.objects.get(user_id=requested_user.id)
                    s.delete()
                else:
                    s = Student.objects.get(user_id=requested_user.id)
                    p = Panel.objects.get(user_id=requested_user.id)
                    pr = Project.objects.get(user_id=requested_user.id)
                    s.delete()
                    p.delete()
                    pr.delete()
                r_user.delete()
                User.objects.get(username=username).delete()
                subject = "Registration Rejected"
                msg = "Hi, " + r_user.first_name + ' ' + r_user.last_name + ', your registration request has not approved.Please contact Project coordinator.'
            elif 'approve':
                r_user.admin_approve = True;
                current_site = get_current_site(request)
                subject = "Registration Completed"
                msg = "Hi, " + r_user.first_name + ' ' + r_user.last_name + ', your registration request has been approved.\nPlease click the following link to confirm your registration:\nhttp://'+ str(current_site)
                r_user.save()
            email_sent(subject,msg,r_user.email)
            return redirect('admin')
        else:
            messages.warning(request, 'You cant access this page')
            return redirect('login_view')
    else:
        messages.warning(request,'something went wrong')
        return redirect('login_view')



#this view used to send the mail
def email_sent(subject,msg,mail):
    try:
        send_mail(subject, msg, 'PAS Team',[mail], fail_silently=False)
        return True
    except:
        return False

#this view is used to add a student  through admin side
@csrf_exempt
@login_required(login_url=LOGIN_REDIRECT_URL)
def student_profile(request):
    username = request.session.get('username')
    check_admin = User.objects.get(username=username).is_superuser
    if check_admin:
        if request.method=='POST':
            first_name=request.POST['firstName']
            last_name=request.POST['lastName']
            regno=request.POST['registerno']
            email=request.POST['email']
            password=request.POST['password']
            staff_status=False
            if User.objects.filter(username=regno).exists():
                messages.warning(request,'Student with this register number is already registered with this system')
                return redirect('admin_student')
            else:
                s=User.objects.create_user(first_name=first_name,last_name=last_name,username=regno,email=email,password=password,is_staff=staff_status,is_active=True)
                s.save()
                p=Profile.objects.get(user_id=s.id)
                p.signup_confirmation=True
                p.admin_approve = True
                p.first_name=first_name
                p.last_name=last_name
                p.email=email
                p.save()
                msg='The student with the register no '+regno +' has been added. He/she can use register number as username.'
                messages.success(request,msg)
                return redirect('admin_student')
        else:
            return render(request,'administrator/student_profile.html')
    else:
        messages.warning(request,'You cant access this page')
        return redirect('login_view')

#this view is used to add a staff  through admin side
@csrf_exempt
@login_required(login_url=LOGIN_REDIRECT_URL)
def add_staff(request):
    username = request.session.get('username')
    check_admin = User.objects.get(username=username).is_superuser
    if check_admin:
        if request.method=='POST':
            first_name=request.POST['firstName']
            last_name=request.POST['lastName']
            regno=request.POST['collegeid']
            email=request.POST['email']
            password=request.POST['password']
            staff_status=True
            if User.objects.filter(username=regno).exists():
                messages.warning(request,'Staff with this register number is already registered ')
                return redirect('add_staff')
            else:
                s=User.objects.create_user(first_name=first_name,last_name=last_name,username=regno,email=email,password=password,is_staff=staff_status,is_active=True)
                s.save()
                p=Profile.objects.get(user_id=s.id)
                p.signup_confirmation=True
                p.admin_approve = True
                p.first_name=first_name
                p.last_name=last_name
                p.email=email
                p.save()
                msg=regno +' has been added. He/she can use register number as username.'
                messages.success(request,msg)
                return redirect('add_staff')
        else:
            return render(request,'administrator/add_staff.html')
    else:
        messages.warning(request,'You cant access this page')
        return redirect('login_view')


@csrf_exempt
@login_required(login_url=LOGIN_REDIRECT_URL)
def complete_profile(request):
    username=request.session.get('username')
    user=User.objects.get(username=username)
    if user.is_superuser:
        pass
    else:
        if user.is_staff:
            pass
        else:
            if request.method=='POST':
                admission=request.POST['admissionno']
                rollno=(request.POST['rollno']).upper()
                batch=request.POST['batch']
                country_code=request.POST['countrycode']
                number=request.POST['mobileno']
                mobile=country_code+number
                if Student.objects.filter(admission_no=admission).exists():
                    messages.warning(request,'student with this admission no already registerd')
                    return redirect('complete_profile')
                else:
                    if Student.objects.filter(roll_no=rollno).exists():
                        messages.warning(request, 'student with this roll no already registerd')
                        return redirect('complete_profile')
                    else:
                        stu=Student.objects.get(user_id=user.id)
                        stu.admission_no=admission
                        stu.roll_no=rollno
                        stu.batch=int(batch)
                        stu.mobile_no=mobile
                        stu.save()
                        messages.success(request,'Updated Successfully.You can go back now')
                        return redirect('complete_profile')
            else:
                #messages.warning(request,'something went wrong')
                return render(request, 'student/profile_update.html')


 #this method is used to edit the student profile
@csrf_exempt
@login_required(login_url=LOGIN_REDIRECT_URL)
def profile_edit(request):
    username=request.session.get('username')
    user=User.objects.get(username=username)
    if user.is_superuser:
        if request.method=='POST':
            first_name=request.POST['firstname']
            last_name=request.POST['lastname']
            email=request.POST['email']
            college_id=request.POST['collegeid']
            if User.objects.filter(username=college_id).exists():
                check=User.objects.get(username=college_id)
                if check.id==user.id:
                    user.username = college_id
                    user.first_name = first_name
                    user.last_name = last_name
                    user.email = email
                    user.profile.signup_confirmation=True
                    user.profile.admin_approve = True
                    user.profile.save()
                    user.save()
                    messages.success(request, 'Successfully Updated')
                    return redirect('admin_profile_edit')
                else:
                    messages.warning(request,'Staff with this ID is already registered')
                    return redirect('admin_profile_edit')
            else:
                user.username=college_id
                user.first_name=first_name
                user.last_name=last_name
                user.email=email
                user.profile.signup_confirmation = True
                user.profile.admin_approve = True
                user.profile.save()
                user.save()
                messages.success(request, 'Successfully Updated')
                return redirect('admin_profile_edit')
        else:
            context = {
                'user': user
            }
            return render(request, 'administrator/admin_edit_profile.html', context)

    elif user.is_staff:
        if request.method=='POST':
            first_name = request.POST['firstname']
            last_name = request.POST['lastname']
            email = request.POST['email']
            user.first_name = first_name
            user.last_name = last_name
            user.email = email
            user.profile.save()
            user.save()
            messages.success(request, 'Successfully Updated')
            return redirect('staff_profile_edit')
        else:
            context = {
                'user': user
            }
            return render(request, 'staff/staff_edit_profile.html', context)
    else:
        if request.method=='POST':
            first_name=request.POST['firstname']
            last_name=request.POST['lastname']
            admission_no = request.POST['admissionno']
            rollno = (request.POST['rollno']).upper()
            email=request.POST['email']
            batch=int(request.POST['batch'])
            country=request.POST['countrycode']
            mob = request.POST['mobileno']
            mobile=country+mob
            user.email=email
            user.first_name=first_name
            user.last_name=last_name
            user.save()
            st=Student.objects.get(user_id=user.id)
            st.admission_no=admission_no
            st.roll_no=rollno
            st.batch=batch
            st.mobile_no=mobile
            st.save()
            messages.success(request,'updated successfully')
            return redirect('student_profile_edit')
        else:
            context={
                'user':user
            }
            return render(request,'student/edit_profile.html',context)


@csrf_exempt
@login_required(login_url=LOGIN_REDIRECT_URL)
def change_password(request):
    username=request.session.get('username')
    user=User.objects.get(username=username)
    if user.is_superuser:
        if request.method == 'POST':
            form = PasswordChangeForm(request.user, request.POST)
            if form.is_valid():
                user = form.save()
                update_session_auth_hash(request, user)  # Important!
                messages.success(request, 'Your password was successfully updated!')
                return redirect('admin_change_password')
            else:
                messages.error(request, 'Please correct the error below.')
        else:
            form = PasswordChangeForm(request.user)
        return render(request, 'administrator/admin_change_password.html', {
            'form': form
        })
    elif user.is_staff:
        if request.method == 'POST':
            form = PasswordChangeForm(request.user, request.POST)
            if form.is_valid():
                user = form.save()
                update_session_auth_hash(request, user)  # Important!
                messages.success(request, 'Your password was successfully updated!')
                return redirect('staff_change_password')
            else:
                messages.error(request, 'Please correct the error below.')
        else:
            form = PasswordChangeForm(request.user)
        return render(request, 'staff/staff_change_password.html', {
            'form': form
        })
    else:
        if request.method == 'POST':
            form = PasswordChangeForm(request.user, request.POST)
            if form.is_valid():
                user = form.save()
                update_session_auth_hash(request, user)  # Important!
                messages.success(request, 'Your password was successfully updated!')
                return redirect('student_change_password')
            else:
                messages.error(request, 'Please correct the error below.')
        else:
            form = PasswordChangeForm(request.user)
        return render(request, 'student/student_change_password.html', {
            'form': form
        })



#this view used to upload abstract
@csrf_exempt
@login_required(login_url=LOGIN_REDIRECT_URL)
def upload_abstract(request):
    username=request.session.get('username')
    user=User.objects.get(username=username)
    if user.is_superuser:
        if request.method=='POST':
            regno=request.POST['registerno']
            try:
                student=User.objects.get(username=regno)
                if student.panel.admin_status!='uploaded' and  student.panel.admin_status!='approved':
                    topic=request.POST['topic']
                    domain = request.POST['domain']
                    abstract=request.FILES['abstract']
                    try:
                        #check=request.POST['checkbox']
                        basepaper=request.FILES['basepaper']
                        student.project.basepaper=basepaper
                    except:
                        pass
                    student.project.abstract=abstract
                    student.project.topic=topic
                    student.project.domain=domain
                    student.panel.admin_status='uploaded'
                    student.panel.save()
                    student.project.save()
                    messages.success(request,'uploaded succesfully')
                else:
                    messages.warning(request, 'He/she is already uploaded')
            except:
                messages.warning(request,'is it a valid register no ?')
            return redirect('/admin/student/add_abstract')
        else:
            return render(request,'administrator/uplod_abstract.html')
    elif not user.is_staff:
        if request.method=='POST':
            admin_status=Panel.objects.get(user_id=user.id).admin_status
            if admin_status != 'approved' and admin_status != 'uploaded' and admin_status != 'waiting':
                try:
                    last_date = Date.objects.get(title='abstract', batch=user.student.batch).last_date.strftime("%Y-%m-%d %H:%M:%S")
                except:
                    last_date = False
                if last_date:
                    date_now = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
                    if date_now < last_date:
                        topic=request.POST['topic']
                        domain = request.POST['domain']
                        abstract=request.FILES['abstract']
                        basepaper = request.FILES['basepaper']
                        if admin_status == 'rejected':
                            old_abstract=user.project.abstract
                            old_basepaper = user.project.basepaper
                            os.remove(old_abstract.path)
                            os.remove(old_basepaper.path)
                        user.project.abstract=abstract
                        user.project.basepaper=basepaper
                        user.project.topic=topic
                        user.project.domain=domain
                        user.panel.admin_status='uploaded'
                        user.project.save()
                        user.panel.save()
                        messages.success(request, 'Successfully uploaded')
                        return redirect('/student/upload_abstract')
                    else:
                        messages.warning(request, 'Uploading time ended')
                        return redirect('/student/upload_abstract')
                else:
                    messages.warning(request, 'Uploading time is not started yet')
                    return redirect('/student/upload_abstract')
            else:
                messages.warning(request, 'Abstract is already uploaded')
                return redirect('/student/upload_abstract')
        else:
            return render(request,'student/upload_abstract.html')


#this view used to upload report
@csrf_exempt
@login_required(login_url=LOGIN_REDIRECT_URL)
def upload_report(request):
    username=request.session.get('username')
    user=User.objects.get(username=username)
    if user.is_superuser:
        if request.method == 'POST':
            student=request.POST['registerno']
            try:
                stu=User.objects.get(username=username)
                if stu.panel.admin_status=='approved':
                    report=request.FILES['report']
                    stu.project.report=report
                    stu.project.save()
                    messages.success(request,'successfully uploaded')
                else:
                    messages.warning(request, 'His/Her Abstract not approved yet')
            except:
                messages.warning(request, 'Please check its a valid register number')
            return redirect('/admin/student/add_report')
        else:
            return render(request,'administrator/uplod_report.html')
    elif not user.is_staff:
        if request.method=='POST':
            admin_status = Panel.objects.get(user_id=user.id).admin_status
            if admin_status == 'approved':
                try:
                    last_date = Date.objects.get(title='report', batch=user.student.batch).last_date.strftime(
                        "%Y-%m-%d %H:%M:%S")
                except:
                    last_date = False
                if last_date:
                    date_now = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
                    if date_now < last_date:
                        report = request.FILES['report']
                        user.project.report = report
                        user.project.save()
                        messages.success(request, 'Successfully uploaded')
                        return redirect('/student/upload_report')
                    else:
                        messages.warning(request, 'Uploading time ended')
                        return redirect('/student/upload_report')
                else:
                    messages.warning(request, 'Uploading time is not started yet')
                    return redirect('/student/upload_report')
            else:
                messages.warning(request, 'Your project topic is not approved yet')
                return redirect('/student/upload_report')
        else:
            return render(request,'student/upload_report.html')



# this view used to create abstract
@csrf_exempt
@login_required(login_url=LOGIN_REDIRECT_URL)
def create_abstract(request):
    username = request.session.get('username')
    user = User.objects.get(username=username)
    if not user.is_staff:
        if request.method == 'POST':
            pass
        else:
            return render(request, 'student/create_abstract.html')
    else:
        messages.success(request,'You have no access to this page')
        return redirect('login_view')




#this view is used to create abstract pdf
class Pdf(View):
    def post(self, request):
        username = request.session.get('username')
        user=User.objects.get(username=username)
        if not user.is_staff:
            if request.method=='POST':
                topic=request.POST['topic']
                introduction=request.POST['introduction']
                objecives=request.POST['objectives']
                motivation=request.POST['motivation']
                problem=request.POST['problem']
                frontend=request.POST['frontend']
                backend=request.POST['backend']
                ide=request.POST['ide']
                database=request.POST['database']
                params = {'topic':topic,'introduction':introduction,'objectives':objecives,'motivation':motivation,'problem':problem,'frontend':frontend,
                          'backend':backend,'ide':ide,'database':database,'user':user}
                return Render.render('student/abstract.html',params)
            else:
                messages.warning(request,'Something went wrong')
                return redirect('create_abstract')
        else:
            messages.warning(request,'you have no access to this page')
            return redirect('login_view')





#this view is used to edit a student  through admin side
@csrf_exempt
@login_required(login_url=LOGIN_REDIRECT_URL)
def edit_student(request):
    username = request.session.get('username')
    check_admin = User.objects.get(username=username).is_superuser
    if check_admin:
        try:
            students=User.objects.filter(is_staff=False).order_by('username')
        except:
            students=False
        context={
            'students':students
        }
        return render(request,'administrator/edit_student.html',context)
    else:
        messages.warning(request,'You cant access this page')
        return redirect('login_view')


#this view is used to edit a staff  through admin side
@csrf_exempt
@login_required(login_url=LOGIN_REDIRECT_URL)
def edit_staff(request):
    username = request.session.get('username')
    check_admin = User.objects.get(username=username).is_superuser
    if check_admin:
        try:
            students=User.objects.filter(is_superuser=False,is_staff=True).order_by('username')
        except:
            students=False
        context={
            'students':students
        }
        return render(request,'administrator/edit_staff.html',context)
    else:
        messages.warning(request,'You cant access this page')
        return redirect('login_view')



#this view is used to edit a staff  through admin side
@csrf_exempt
@login_required(login_url=LOGIN_REDIRECT_URL)
def staff_edit(request,username):
    usernam = request.session.get('username')
    check_admin = User.objects.get(username=usernam).is_superuser
    if check_admin:
        if request.method=='POST':
            user = User.objects.get(username=username)
            context = {
                'user': user
            }
            if 'edit' in request.POST:
                return render(request, 'administrator/edit_staff_detail.html', context)
            elif 'delete' in request.POST:
                Staff.objects.get(user_id=user.id).delete()
                User.objects.get(username=username).delete()
                msg='Successfully deleted ('+username+').'
                messages.success(request,msg)
                return redirect('edit_staff')
        else:
            messages.warning(request, 'Something went wrong')
            return redirect('login_view')
    else:
            messages.warning(request,'You cant access this page')
            return redirect('login_view')


#this view is used to  update edited staff details  through admin side
@csrf_exempt
@login_required(login_url=LOGIN_REDIRECT_URL)
def staff_edit_update(request,username):
    usernam = request.session.get('username')
    check_admin = User.objects.get(username=usernam).is_superuser
    if check_admin:
        if request.method=='POST':
            user = User.objects.get(username=username)
            regno=request.POST['registerno']
            first=request.POST['firstname']
            last = request.POST['lastname']
            email=request.POST['email']
            if User.objects.filter(username=regno).exists():
                check = User.objects.get(username=regno)
                if check.id == user.id:
                    user.username=regno
                    user.first_name=first
                    user.last_name=last
                    user.email=email
                    user.save()
                    msg=username+" updated Successfully."
                    messages.success(request,msg)
                    return redirect('edit_staff')
                else:
                    msg='This ID ('+regno+') is already exist with another staff'
                    messages.warning(request,msg)
                    return redirect('edit_staff')

            else:
                user.username = regno
                user.first_name = first
                user.last_name = last
                user.email = email
                user.save()
                msg = username + " updated Successfully."
                messages.success(request, msg)
                return redirect('edit_staff')

        else:
            messages.warning(request, 'Something went wrong')
            return redirect('login_view')
    else:
            messages.warning(request,'You cant access this page')
            return redirect('login_view')




#this view is used to  create panel batch selection  through admin side
@csrf_exempt
@login_required(login_url=LOGIN_REDIRECT_URL)
def create_panel(request):
    username = request.session.get('username')
    check_admin = User.objects.get(username=username).is_superuser
    if check_admin:
        if request.method=='POST':
            batch=request.POST['batch']
            url='create/'+batch
            return redirect(url)
        else:
            return render(request,'administrator/create_panel_batch.html')
    else:
        messages.warning(request,"you can't access this page")
        return redirect('login_view')




#this view is used to  create panel   through admin side
@csrf_exempt
@login_required(login_url=LOGIN_REDIRECT_URL)
def create_panel_batch(request,batch):
    username = request.session.get('username')
    check_admin = User.objects.get(username=username).is_superuser
    s=[]
    p=[]
    if check_admin:
        if request.method=='POST':
            guide=request.POST['guide']
            staff1=request.POST['staff1']
            staff2=request.POST['staff2']
            staff3=request.POST['staff3']
            students=request.POST.getlist('checks')
            #print(students)
            for i in students:
                user=User.objects.get(id=i)
                user.panel.guide=guide
                user.panel.panel1 = staff1
                user.panel.panel2 = staff2
                user.panel.panel3 = staff3
                user.panel.save()
            batch=Student.objects.get(user_id=students[0]).batch
            url='/admin/panel/create/'+str(batch)
            return redirect(url)


        else:
            stu=Student.objects.filter(batch=batch)
            pan=Panel.objects.filter(guide=0)
            sta=User.objects.filter(is_superuser=False,is_staff=True,is_active=True)
            if stu and pan:
                for i in stu:
                    #print(i.user_id)
                    s.append(i.user_id)
                for k in pan:
                    #print(k.user_id)
                    p.append(k.user_id)
                #print('s')
                #print(s)
                #print('p')
                #print(p)
                s=set(p)&set(s)
                #print(s)
                p = []
                for j in s:
                    p.append(User.objects.get(id=j))
                #print(p)


            context={
                'batch':batch,'users':p,'staffs':sta
            }
            return render(request,'administrator/create_panel.html',context)
    else:
        messages.warning(request,"you can't access this page")
        return redirect('login_view')



#this view is used to  activate the user  through admin side
@csrf_exempt
@login_required(login_url=LOGIN_REDIRECT_URL)
def activate_user(request):
    username = request.session.get('username')
    check_admin = User.objects.get(username=username).is_superuser
    if check_admin:
        if request.method=='POST':
            usname=request.POST['registerno']
            try:
                if User.objects.get(username=usname).is_superuser:
                    messages.warning(request, "oops its Admin's ID")
                    return redirect('student_activate')
                elif User.objects.get(username=usname).is_active:
                    messages.warning(request,'He/She already activated')
                    return redirect('student_activate')
                else:
                    user=User.objects.get(username=usname)
                    user.is_active=True
                    user.save()
                    messages.success(request,'Activated Successfully')
                    return redirect('student_activate')
            except:
                messages.warning(request, 'Please Check the entered Register No/College ID is valid')
                return redirect('student_activate')
        else:
            context={'subject':'Activate'}
            return render(request,'administrator/activate_deactivate.html',context)
    else:
        messages.warning(request,"you can't access this page")
        return redirect('login_view')


@csrf_exempt
@login_required(login_url=LOGIN_REDIRECT_URL)
def deactivate_user(request):
    username = request.session.get('username')
    check_admin = User.objects.get(username=username).is_superuser
    if check_admin:
        if request.method=='POST':
            usname = request.POST['registerno']
            try:
                if User.objects.get(username=usname).is_superuser:
                    messages.warning(request, "oops its Admin's ID")
                    return redirect('student_deactivate')

                elif not User.objects.get(username=usname).is_active:
                    messages.warning(request, 'He/She already Deactivated')
                    return redirect('student_deactivate')
                else:
                    user = User.objects.get(username=usname)
                    user.is_active = False
                    user.save()
                    messages.success(request, 'Deactivated Successfully')
                    return redirect('student_deactivate')
            except:
                messages.warning(request, 'Please Check the entered Register No/College ID is valid')
                return redirect('student_activate')
        else:
            context={'subject':'Deactivate'}
            return render(request,'administrator/activate_deactivate.html',context)
    else:
        messages.warning(request,"you can't access this page")
        return redirect('login_view')



#this view is used to  edit panel batch selection  through admin side
@csrf_exempt
@login_required(login_url=LOGIN_REDIRECT_URL)
def edit_panel(request):
    username = request.session.get('username')
    check_admin = User.objects.get(username=username).is_superuser
    if check_admin:
        if request.method=='POST':
            batch=request.POST['batch']
            url='edit/'+batch
            return redirect(url)
        else:
            context={'page':'editpanel'}
            return render(request,'administrator/create_panel_batch.html',context)
    else:
        messages.warning(request,"you can't access this page")
        return redirect('login_view')



#this view is used to  edit panel   through admin side
@csrf_exempt
@login_required(login_url=LOGIN_REDIRECT_URL)
def edit_panel_batch(request,batch):
    username = request.session.get('username')
    check_admin = User.objects.get(username=username).is_superuser
    s=[]
    p=[]
    if check_admin:
        if request.method=='POST':
            pass
        else:
            stu = Student.objects.filter(batch=batch)
            pan = Panel.objects.filter(~Q(guide=0))
            #sta = User.objects.filter(is_superuser=False, is_staff=True)
            if stu and pan:
                for i in stu:
                    # print(i.user_id)
                    s.append(i.user_id)
                for k in pan:
                    # print(k.user_id)
                    p.append(k.user_id)
                # print('s')
                # print(s)
                # print('p')
                # print(p)
                s = set(p) & set(s)
                # print(s)
                p = []
                for j in s:
                    p.append(User.objects.get(id=j))
                # print(p)

            context = {
                'batch': batch, 'users': p
            }
            return render(request,'administrator/edit_panel.html',context)
    else:
        messages.warning(request,"you can't access this page")
        return redirect('login_view')




#this view is used to  edit panel batch student selection  through admin side
@csrf_exempt
@login_required(login_url=LOGIN_REDIRECT_URL)
def edit_panel_student(request,batch,username):
    usernam = request.session.get('username')
    check_admin = User.objects.get(username=usernam).is_superuser
    if check_admin:
        if request.method=='POST':
            user = User.objects.get(username=username)
            if 'edit' in request.POST:
                guide=User.objects.get(id=user.panel.guide)
                panel1=User.objects.get(id=user.panel.panel1)
                panel2 = User.objects.get(id=user.panel.panel2)
                panel3 = User.objects.get(id=user.panel.panel3)
                staffs=User.objects.filter(is_superuser=False,is_active=True,is_staff=True)
                context={'guide':guide,'panel1':panel1,'panel2':panel2,'panel3':panel3,'staffs':staffs,'batch':batch,'username':username}
                return render(request,'administrator/edit_panel_detail.html',context)
            elif 'delete' in request.POST:
                user.panel.guide=0
                user.panel.panel1 = 0
                user.panel.panel2 = 0
                user.panel.panel3 = 0
                user.panel.save()
                msg='Panel allocated to '+username+" ,deleted successfully"
                messages.success(request,msg)
                url='/admin/panel/edit/'+batch
                return redirect(url)
    else:
        messages.warning(request,"you can't access this page")
        return redirect('login_view')



#this view is used to confirm for the edit panel batch student selection  through admin side
@csrf_exempt
@login_required(login_url=LOGIN_REDIRECT_URL)
def edit_panel_student_confirm(request,batch,username):
    usernam = request.session.get('username')
    check_admin = User.objects.get(username=usernam).is_superuser
    if check_admin:
        if request.method=='POST':
            user = User.objects.get(username=username)
            guide=request.POST['guide']
            staff1=request.POST['staff1']
            staff2 = request.POST['staff2']
            staff3 = request.POST['staff3']
            user.panel.guide=guide
            user.panel.panel1=staff1
            user.panel.panel2=staff2
            user.panel.panel3=staff3
            user.panel.save()
            msg='panel of '+username+' edited successfully'
            messages.success(request,msg)
            url = '/admin/panel/edit/' + batch
            return redirect(url)

    else:
        messages.warning(request,"you can't access this page")
        return redirect('login_view')


#this view is used to  change admin
@csrf_exempt
@login_required(login_url=LOGIN_REDIRECT_URL)
def change_admin(request):
    usernam = request.session.get('username')
    check_admin = User.objects.get(username=usernam)
    if check_admin.is_superuser:
        if request.method=='POST':
            new_admin=request.POST['admin']
            new=User.objects.get(id=new_admin)
            new.is_superuser=True
            new.save()
            check_admin.is_superuser=False
            check_admin.save()
            logout(request)
            messages.success(request,'Admin changed successfully,You must login now')
            return redirect('login_view')
        else:
            staffs=User.objects.filter(is_superuser=False,is_active=True,is_staff=True)
            context={'staffs':staffs}
            return render(request,'administrator/change_admin.html',context)

    else:
        messages.warning(request,"you can't access this page")
        return redirect('login_view')


#this view is used to congif email from adminside
@csrf_exempt
@login_required(login_url=LOGIN_REDIRECT_URL)
def signup_confirm(request):
    username=request.session.get('username')
    check_admin=User.objects.get(username=username)
    if check_admin.is_superuser:
        first_name=check_admin.first_name
        try:
            user_to_approve = Profile.objects.filter(signup_confirmation=False)
            a = []
            for user in user_to_approve:
                a.append(user.id)
            signup_approval= User.objects.filter(id__in=a)
        except:
            signup_approval= False
        context={
            'signup_approval':signup_approval,
            'name':first_name
        }
        return render(request,'administrator/signup_confirm.html',context)
    else:
        messages.warning(request,'You cant access this page')
        return redirect('login_view')

# this view is associated with dynamic url which helps to manage  signup requests in admin side
@login_required(login_url=LOGIN_REDIRECT_URL)
def signup_approve(request,username):
    if request.method=='POST':
        #print('Inside Post')
        logged_user = request.session.get('username')
        check_admin = User.objects.get(username=logged_user).is_superuser   #ensuring that logged in person is admin
        if check_admin:
            requested_user=User.objects.get(username=username)  #getting the requested user from User model
            #print(requested_user)
            r_user=Profile.objects.get(id=requested_user.id)    #getting the row of that requested user from Profile model
            if 'reject' in request.POST:
                if requested_user.is_staff == True:
                    s = Staff.objects.get(user_id=requested_user.id)
                    s.delete()
                else:
                    s = Student.objects.get(user_id=requested_user.id)
                    p = Panel.objects.get(user_id=requested_user.id)
                    pr = Project.objects.get(user_id=requested_user.id)
                    s.delete()
                    p.delete()
                    pr.delete()
                r_user.delete()
                User.objects.get(username=username).delete()
            elif 'approve':
                r_user.signup_confirmation = True;
                r_user.save()
                messages.success(request,'successfull')
            return redirect('signup_confirm')
        else:
            messages.warning(request, 'You cant access this page')
            return redirect('login_view')
    else:
        messages.warning(request,'something went wrong')
        return redirect('login_view')


#this view is used to set abstract date in admin side
@csrf_exempt
@login_required(login_url=LOGIN_REDIRECT_URL)
def abstract_date(request):
    username = request.session.get('username')
    check_admin = User.objects.get(username=username).is_superuser
    if check_admin:
        if request.method=='POST':
             batch=request.POST['batch']
             url='abstract_date/'+batch
             return redirect(url)
        else:
            context={'page':'abstract_date'}
            return render(request,'administrator/create_panel_batch.html',context)
    else:
        messages.warning(request,'You cant access this page')
        return redirect('login_view')


#this view is used to set report date in admin side
@csrf_exempt
@login_required(login_url=LOGIN_REDIRECT_URL)
def report_date(request):
    username = request.session.get('username')
    check_admin = User.objects.get(username=username).is_superuser
    if check_admin:
        if request.method=='POST':
            batch = request.POST['batch']
            url = 'report_date/' + batch
            return redirect(url)
        else:
            context={'page':'report_date'}
            return render(request,'administrator/create_panel_batch.html',context)
    else:
        messages.warning(request,'You cant access this page')
        return redirect('login_view')

#this view is used to set confirm abstract date in admin side
@csrf_exempt
@login_required(login_url=LOGIN_REDIRECT_URL)
def abstract_date_set(request,batch):
    username = request.session.get('username')
    check_admin = User.objects.get(username=username).is_superuser
    if check_admin:
        if request.method=='POST':
             last_date=request.POST['abstract_date']
             check=request.POST['abstract_dat']
             #print(check)
             if check == 'False':
                 #print('entered')
                 dat = Date.objects.create(title='abstract', last_date=last_date, type='main', batch=batch)
             else:
                 dat = Date.objects.get(batch=batch, title='abstract')
                 dat.last_date = last_date
                 dat.save()
             messages.success(request,'Added/Updated Successfully')
             url='/admin/abstract_date/'+batch
             return redirect(url)
        else:
            try:
                date=Date.objects.get(batch=batch,title='abstract').last_date
            except:
                date=False
            context={'page':'abstract_date','batch':batch,'date':date}
            return render(request,'administrator/date_set_confirm.html',context)
    else:
        messages.warning(request,'You cant access this page')
        return redirect('login_view')


#this view is used to set confirm report date in admin side
@csrf_exempt
@login_required(login_url=LOGIN_REDIRECT_URL)
def report_date_set(request,batch):
    username = request.session.get('username')
    check_admin = User.objects.get(username=username).is_superuser
    if check_admin:
        if request.method == 'POST':
            last_date = request.POST['report_date']
            check = request.POST['report_dat']
            # print(check)
            if check == 'False':
                # print('entered')
                dat = Date.objects.create(title='report', last_date=last_date, type='main', batch=batch)
            else:
                dat = Date.objects.get(batch=batch, title='report')
                dat.last_date = last_date
                dat.save()
            messages.success(request, 'Added/Updated Successfully')
            url = '/admin/report_date/' + batch
            return redirect(url)
        else:
            try:
                date = Date.objects.get(batch=batch, title='report').last_date
            except:
                date = False
            context = {'page': 'report_date', 'batch': batch, 'date': date}
            return render(request, 'administrator/date_set_confirm.html', context)
    else:
        messages.warning(request,'You cant access this page')
        return redirect('login_view')



#this view is used to upload sample abstract
@csrf_exempt
@login_required(login_url=LOGIN_REDIRECT_URL)
def sample_abstract(request):
    username = request.session.get('username')
    check_admin = User.objects.get(username=username).is_superuser
    if check_admin:
        if request.method == 'POST':
            document=request.FILES['abstract']
            try:
                abstract = Document.objects.get(title='abstract')
            except:
                abstract = False
            if abstract:
                os.remove(abstract.doc.path)
                abstract.doc=document
                abstract.save()
                msg='Updated Successfully'
            else:
                newone=Document.objects.create(title='abstract',doc=document)
                msg = 'Added Successfully'
            messages.success(request,msg)
            return redirect('sample_abstract')
        else:
            try:
                abstract = Document.objects.get(title='abstract').doc
            except:
                abstract = False
            context = {'abstract':abstract}
            return render(request, 'administrator/sample_abstract.html', context)
    else:
        messages.warning(request,'You cant access this page')
        return redirect('login_view')



#this view is used to see under guidance students in staff side
@csrf_exempt
@login_required(login_url=LOGIN_REDIRECT_URL)
def under_guidance(request):
    username = request.session.get('username')
    user = User.objects.get(username=username)
    if user.is_staff and not user.is_superuser:
        if request.method == 'POST':
            batch=request.POST['batch']
            url='/staff/req_under_guidance/'+str(batch)
            return redirect(url)
        else:
            context={'page':'under_guidance'}
            return render(request, 'staff/batch_selection.html',context)
    else:
        messages.warning(request,'You cant access this page')
        return redirect('login_view')


#this view is used to see under guidance students batchwise in staff side
@csrf_exempt
@login_required(login_url=LOGIN_REDIRECT_URL)
def under_guidance_batch(request,batch):
    username = request.session.get('username')
    user = User.objects.get(username=username)
    if user.is_staff and not user.is_superuser:
        batch_student=Student.objects.filter(batch=batch) and Panel.objects.filter(guide=user.id,admin_status='uploaded',guide_status='')
        #for i in batch_student:
            #print(i)
        context={'page':'under_guidance','students':batch_student,'batch':batch}
        return render(request, 'staff/student_list.html',context)
    else:
        messages.warning(request,'You cant access this page')
        return redirect('login_view')




#this view is used to see under guidance students batchwise with username in staff side
@csrf_exempt
@login_required(login_url=LOGIN_REDIRECT_URL)
def under_guidance_batch_username(request,batch,username):
    usernam = request.session.get('username')
    user = User.objects.get(username=usernam)
    if user.is_staff and not user.is_superuser:
        if request.method=='POST':
            student=User.objects.get(username=username)
            page=request.POST['page']
            if page == 'under_guidance':
                action = '/staff/req_under_guidance/'+str(batch)+'/'+username+'/approve_reject'
            elif page == 'under_panel':
                action = '/staff/req_under_panel/' + str(batch) + '/' + username + '/approve_reject'
            context={'student':student,'batch':batch,'action':action,'page':page}
            return render(request, 'staff/student.html',context)
        else:
            messages.warning(request, 'oops something went wrong')
            return redirect('login_view')
    else:
        messages.warning(request,'You cant access this page')
        return redirect('login_view')

#this view is used to approve_reject in staff side
@csrf_exempt
@login_required(login_url=LOGIN_REDIRECT_URL)
def approve_reject(request,batch,username):
    usernam = request.session.get('username')
    user = User.objects.get(username=usernam)
    if user.is_staff :
        if request.method=='POST':
            stu = User.objects.get(username=username)
            if 'reject' in request.POST:
                notes = request.POST['notes']
                if user.is_superuser:
                    stu.panel.admin_status=''
                    stu.panel.Notes = ''
                    stu.panel.guide_status = ''
                    stu.panel.panel1_status = ''
                    stu.panel.panel2_status = ''
                    stu.panel.panel3_status = ''
                    stu.panel.save()
                    reject=stu.project.reject_no
                    reject+=1
                    stu.project.reject_no=reject
                    stu.project.topic=''
                    stu.project.domain=''
                    os.remove(stu.project.abstract.path)
                    if stu.project.basepaper != '':
                        os.remove(stu.project.basepaper.path)
                    stu.project.abstract=''
                    stu.project.basepaper=''
                    stu.project.save()
                    email_sent('Abstract Rejected',notes,stu.email)
                    messages.success(request,'Rejected successfully')
                    url='/admin/approve_reject/'+str(batch)

                else:
                    old_not = stu.panel.Notes
                    notes = old_not + '\n' + user.first_name + user.last_name + ' : ' + notes
                    if stu.panel.guide == user.id:
                        stu.panel.guide_status = 'rejected'
                        stu.panel.Notes = notes
                        stu.panel.save()
                        msg = stu.first_name + ' ' + stu.last_name + "'s abstract rejected"
                        messages.success(request, msg)
                        url = '/staff/req_under_guidance/' + batch
                    else:
                        if stu.panel.panel1 == user.id:
                            stu.panel.panel1_status = 'rejected'
                            stu.panel.Notes = notes
                        elif stu.panel.panel2== user.id:
                            stu.panel.panel2_status = 'rejected'
                            stu.panel.Notes = notes
                        elif stu.panel.panel3== user.id:
                            stu.panel.panel3_status = 'rejected'
                            stu.panel.Notes = notes
                        stu.panel.save()
                        msg = stu.first_name + ' ' + stu.last_name + "'s abstract rejected"
                        messages.success(request, msg)
                        url = '/staff/req_under_panel/' + batch

                return redirect(url)
            else:
                notes = request.POST['notes']
                if user.is_superuser:
                    stu.panel.admin_status='approved'
                    stu.panel.save()
                    email_sent('Abstract Approved',notes,stu.email)
                    messages.success(request,'Approved successfully')
                    url='/admin/approve_reject/'+str(batch)
                else:
                    old_not = stu.panel.Notes
                    notes = old_not + '\n' + user.first_name + user.last_name + ' : ' + notes
                    if stu.panel.guide == user.id:
                        stu.panel.guide_status='approved'
                        stu.panel.Notes=notes
                        stu.panel.save()
                        msg=stu.first_name+' '+stu.last_name+"'s abstract approved"
                        messages.success(request,msg)
                        url='/staff/req_under_guidance/'+batch
                    else:
                        if stu.panel.panel1 == user.id:
                            stu.panel.panel1_status = 'approved'
                            stu.panel.Notes = notes
                        elif stu.panel.panel2== user.id:
                            stu.panel.panel2_status = 'approved'
                            stu.panel.Notes = notes
                        elif stu.panel.panel3== user.id:
                            stu.panel.panel3_status = 'approved'
                            stu.panel.Notes = notes
                        stu.panel.save()
                        msg = stu.first_name + ' ' + stu.last_name + "'s abstract 'approved'"
                        messages.success(request, msg)
                        url = '/staff/req_under_panel/' + batch
                return redirect(url)
        else:
            messages.warning(request, 'oops something went wrong')
            return redirect('login_view')
    else:
        messages.warning(request,'You cant access this page')
        return redirect('login_view')



#this view is used to see under panel students in staff side
@csrf_exempt
@login_required(login_url=LOGIN_REDIRECT_URL)
def under_panel(request):
    username = request.session.get('username')
    user = User.objects.get(username=username)
    if user.is_staff and not user.is_superuser:
        if request.method == 'POST':
            batch=request.POST['batch']
            url='/staff/req_under_panel/'+str(batch)
            return redirect(url)
        else:
            context={'page':'under_panel'}
            return render(request, 'staff/batch_selection.html',context)
    else:
        messages.warning(request,'You cant access this page')
        return redirect('login_view')



#this view is used to see under panel students batchwise in staff side
@csrf_exempt
@login_required(login_url=LOGIN_REDIRECT_URL)
def under_panel_batch(request,batch):
    username = request.session.get('username')
    user = User.objects.get(username=username)
    if user.is_staff and not user.is_superuser:
        batch_student=Student.objects.filter(batch=batch) and (Panel.objects.filter(panel1=user.id,admin_status='uploaded',panel1_status='') or Panel.objects.filter(panel2=user.id,admin_status='uploaded',panel2_status='') or Panel.objects.filter(panel3=user.id,admin_status='uploaded',panel3_status=''))
        #for i in batch_student:
            #print(i)
        context={'page':'under_panel','students':batch_student,'batch':batch}
        return render(request, 'staff/student_list.html',context)
    else:
        messages.warning(request,'You cant access this page')
        return redirect('login_view')


@csrf_exempt
@login_required(login_url=LOGIN_REDIRECT_URL)
def approve_batch(request):
    username = request.session.get('username')
    user = User.objects.get(username=username)
    if user.is_superuser:
        if request.method=='POST':
            batch = request.POST['batch']
            url = '/admin/approve_reject/' + str(batch)
            return redirect(url)
        else:
            context={'page':'approve_page'}
            return render(request,'administrator/create_panel_batch.html',context)
    else:
        messages.warning(request, 'You cant access this page')
        return redirect('login_view')

@csrf_exempt
@login_required(login_url=LOGIN_REDIRECT_URL)
def approve_batch_student(request,batch):
    username = request.session.get('username')
    user = User.objects.get(username=username)
    if user.is_superuser:
        students=Panel.objects.filter(~Q(guide_status='',panel1_status='',panel2_status='',panel3_status=''),admin_status='uploaded')
        context={'students':students,'batch':batch}
        return render(request,'administrator/student_list_show.html',context)
    else:
        messages.warning(request, 'You cant access this page')
        return redirect('login_view')

@csrf_exempt
@login_required(login_url=LOGIN_REDIRECT_URL)
def student_detail_show(request,batch,username):
    usernam = request.session.get('username')
    user = User.objects.get(username=usernam)
    if user.is_superuser:
        if request.method=='POST':
            student=User.objects.get(username=username)
            guide=User.objects.get(id=student.panel.guide)
            panel1=User.objects.get(id=student.panel.panel1)
            panel2 = User.objects.get(id=student.panel.panel2)
            panel3 = User.objects.get(id=student.panel.panel3)
            context={'student':student,'batch':batch,'guide':guide,'panel1':panel1,'panel2':panel2,'panel3':panel3}
            return render(request,'administrator/student_detail.html',context)
        else:
            messages.warning(request, 'Something went wrong')
            return redirect('login_view')
    else:
        messages.warning(request, 'You cant access this page')
        return redirect('login_view')



#this view is used to edit a student  through admin side
@csrf_exempt
@login_required(login_url=LOGIN_REDIRECT_URL)
def student_edit(request,username):
    usernam = request.session.get('username')
    check_admin = User.objects.get(username=usernam).is_superuser
    if check_admin:
        if request.method=='POST':
            student = User.objects.get(username=username)

            if 'edit' in request.POST:
                g = student.panel.guide
                p1 = student.panel.panel1
                p2 = student.panel.panel2
                p3 = student.panel.panel3
                guide=''
                panel1=''
                panel2=''
                panel3=''
                if g != 0 and p1 != 0 and p2 != 0 and p3 != 0:
                    guide = (User.objects.get(id=g).first_name) + ' ' + (User.objects.get(id=g).last_name)
                    panel1 = (User.objects.get(id=p1).first_name) + ' ' + (User.objects.get(id=p1).last_name)
                    panel2 = (User.objects.get(id=p2).first_name) + ' ' + (User.objects.get(id=p2).last_name)
                    panel3 = (User.objects.get(id=p3).first_name) + ' ' + (User.objects.get(id=p3).last_name)
                context = {
                    'user': student,'guide':guide,'panel1':panel1,'panel2':panel2,'panel3':panel3
                }
                return render(request, 'administrator/edit_student_detail.html', context)
            elif 'delete' in request.POST:
                Student.objects.get(user_id=student.id).delete()
                Project.objects.get(user_id=student.id).delete()
                Profile.objects.get(user_id=student.id).delete()
                Panel.objects.get(user_id=student.id).delete()
                User.objects.get(username=username).delete()
                msg='Successfully deleted ('+username+').'
                messages.success(request,msg)
                return redirect('edit_student')
        else:
            messages.warning(request, 'Something went wrong')
            return redirect('login_view')
    else:
            messages.warning(request,'You cant access this page')
            return redirect('login_view')

#this view is used to  update edited student details  through admin side
@csrf_exempt
@login_required(login_url=LOGIN_REDIRECT_URL)
def student_edit_update(request,username):
    usernam = request.session.get('username')
    check_admin = User.objects.get(username=usernam).is_superuser
    if check_admin:
        if request.method=='POST':
            user = User.objects.get(username=username)
            regno=request.POST['registerno']
            email=request.POST['email']
            if User.objects.filter(username=regno).exists():
                check = User.objects.get(username=regno)
                if check.id == user.id:
                    user.username=regno
                    user.email=email
                    user.save()
                    msg=username+" updated Successfully."
                    messages.success(request,msg)
                    return redirect('edit_student')
                else:
                    msg='This Register No ('+regno+') is already exist with another student'
                    messages.warning(request,msg)
                    return redirect('edit_student')
            else:
                user.username = regno
                user.email = email
                user.save()
                msg = username + " updated Successfully."
                messages.success(request, msg)
                return redirect('edit_student')
        else:
            messages.warning(request, 'Something went wrong')
            return redirect('login_view')
    else:
            messages.warning(request,'You cant access this page')
            return redirect('login_view')


class Pdf1(View):
    def post(self, request):
        username = request.session.get('username')
        user = User.objects.get(username=username)
        if not user.is_staff:
            if request.method == 'POST':
                time=(user.project.reject_no)+1
                acadamic=str((user.student.batch)+2)+' - '+str((user.student.batch)+3)
                date=datetime.today().strftime('%d-%m-%Y')
                guide=User.objects.get(id=user.panel.guide).first_name + ' '+User.objects.get(id=user.panel.guide).last_name
                params = {'student': user,'no':time,'acadamic':acadamic,'date':date,'guide':guide,'directory':os. getcwd()}
                return Render.render('approved_copy.html',params)
            else:
                messages.warning(request,'something went wrong')
                return redirect('login_view')

        else:
            messages.warning(request,'you have no permission to access this page')
            return redirect('login_view')


@csrf_exempt
@login_required(login_url=LOGIN_REDIRECT_URL)
def download_page(request):
    usernam = request.session.get('username')
    check_staff = User.objects.get(username=usernam)
    if not check_staff.is_staff:
        context={'student':check_staff}
        return render(request,'student/downloading_page.html',context)
    else:
        messages.warning(request,'You cant access this page')
        return redirect('login_view')



@csrf_exempt
@login_required(login_url=LOGIN_REDIRECT_URL)
def student_list_admin(request):
    usernam = request.session.get('username')
    user = User.objects.get(username=usernam)
    if user.is_superuser:
        if request.method=='POST':
            batch=request.POST['batch']
            url='/admin/download/student_list/'+str(batch)
            return redirect(url)
        else:
            context={'page':'student_list'}
            return render(request,'administrator/download_batch.html',context)
    else:
        messages.warning(request, 'You cant access this page')
        return redirect('login_view')

class Pdf2(View):
    def get(self, request,batch):
        username = request.session.get('username')
        user = User.objects.get(username=username)
        if user.is_superuser:
            students=User.objects.filter(is_staff=False).order_by('username') and Student.objects.filter(batch=batch)
            print(students)
            params = {'students': students,'subject':'Regitered students ','batch':batch,'directory':os. getcwd()}
            return Render.render('administrator/download_student_list.html',params)
        else:
            messages.warning(request,'you have no permission to access this page')
            return redirect('login_view')



@csrf_exempt
@login_required(login_url=LOGIN_REDIRECT_URL)
def my_panel(request):
    usernam = request.session.get('username')
    user = User.objects.get(username=usernam)
    if not user.is_staff:
        guide=''
        panel1=''
        panel2=''
        panel3=''
        g=Panel.objects.get(user_id=user.id).guide
        p1=Panel.objects.get(user_id=user.id).panel1
        p2=Panel.objects.get(user_id=user.id).panel2
        p3=Panel.objects.get(user_id=user.id).panel3
        if g != 0 and  p1 != 0 and p2 !=0 and p3 != 0:
            guide=User.objects.get(id=g).first_name+' '+User.objects.get(id=g).last_name
            panel1=User.objects.get(id=p1).first_name+' '+User.objects.get(id=p1).last_name
            panel2=User.objects.get(id=p2).first_name+' '+User.objects.get(id=p2).last_name
            panel3=User.objects.get(id=p3).first_name+' '+User.objects.get(id=p3).last_name
        context={'guide':guide,'panel1':panel1,'panel2':panel2,'panel3':panel3}
        return render(request,'student/my_panel.html',context)
    else:
        messages.warning(request, 'You cant access this page')
        return redirect('login_view')















