# Generated by Django 2.0 on 2020-06-01 12:02

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0002_auto_20200601_1157'),
    ]

    operations = [
        migrations.AlterField(
            model_name='student',
            name='batch',
            field=models.IntegerField(blank=True),
        ),
    ]
